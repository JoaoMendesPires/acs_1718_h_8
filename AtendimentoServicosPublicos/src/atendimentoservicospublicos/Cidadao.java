/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atendimentoservicospublicos;

/**
 *
 * @author João Pires <your.name at your.org>
 */
public class Cidadao {

    private int contribuinte;
    private String nome;
    private String email;
    private String codigoPostal;
    private int numReparticao;

    private static final int CONTRIBUINTE_POR_OMISSAO = 0;
    private static final String NOME_POR_OMISSAO = "";
    private static final String EMAIL_POR_OMISSAO = "";
    private static final String CODIGO_POSTAL_POR_OMISSAO = "";
    private static final int NUM_REPARTICAO_POR_OMISSAO = 0;

    public Cidadao(int contribuinte, String nome, String email, String codigoPostal, int numReparticao) {
        this.contribuinte = contribuinte;
        this.nome = nome;
        this.email = email;
        this.codigoPostal = codigoPostal;
        this.numReparticao = numReparticao;
    }

    public Cidadao(String codigoPostal, int numReparticao) {
        contribuinte = CONTRIBUINTE_POR_OMISSAO;
        nome = NOME_POR_OMISSAO;
        email = EMAIL_POR_OMISSAO;
        this.codigoPostal = codigoPostal;
        this.numReparticao = numReparticao;
    }

    public Cidadao() {
        contribuinte = CONTRIBUINTE_POR_OMISSAO;
        nome = NOME_POR_OMISSAO;
        email = EMAIL_POR_OMISSAO;
        codigoPostal = CODIGO_POSTAL_POR_OMISSAO;
        numReparticao = NUM_REPARTICAO_POR_OMISSAO;
    }

    /**
     * @return the contribuinte
     */
    public int getContribuinte() {
        return contribuinte;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return the codigoPostal
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * @return the numReparticao
     */
    public int getNumReparticao() {
        return numReparticao;
    }

    /**
     * @param contribuinte the contribuinte to set
     */
    public void setContribuinte(int contribuinte) {
        this.contribuinte = contribuinte;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param codigoPostal the codigoPostal to set
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * @param numReparticao the numReparticao to set
     */
    public void setNumReparticao(int numReparticao) {
        this.numReparticao = numReparticao;
    }

    @Override
    public String toString() {
        return "Cidadao{" + "contribuinte=" + contribuinte + ", nome=" + nome + ", email=" + email + ", codigoPostal=" + codigoPostal + ", numReparticao=" + numReparticao + '}';
    }

}
