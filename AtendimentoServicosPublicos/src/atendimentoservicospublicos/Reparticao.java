/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atendimentoservicospublicos;

import java.util.ArrayList;

/**
 *
 * @author João Pires <your.name at your.org>
 */
public class Reparticao {

    private String cidade;
    private int numReparticao;
    private String codigoPostal;
    private ArrayList<Servico> servicos;

    private static final String CIDADE_POR_OMISSAO = "";
    private static final int NUM_REPARTICAO_POR_OMISSAO = 0;
    private static final String CODIGO_POSTAL_POR_OMISSAO = "";

    public Reparticao(String cidade, int numReparticao, String codigoPostal) {
        this.cidade = cidade;
        this.numReparticao = numReparticao;
        this.codigoPostal = codigoPostal;
        this.servicos = new ArrayList<>();
    }

    public Reparticao() {
        cidade = CIDADE_POR_OMISSAO;
        numReparticao = NUM_REPARTICAO_POR_OMISSAO;
        codigoPostal = CODIGO_POSTAL_POR_OMISSAO;
        this.servicos = new ArrayList();
    }

    /**
     * @return the cidade
     */
    public String getCidade() {
        return cidade;
    }

    public ArrayList getServicos() {
        return servicos;
    }

    public void addServico(String idServico) {
        servicos.add(new Servico(idServico));
    }

    public void setServicos(ArrayList servicos) {
        this.servicos = servicos;
    }

    /**
     * @return the numReparticao
     */
    public int getNumReparticao() {
        return numReparticao;
    }

    /**
     * @return the codigoPostal
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * @param cidade the cidade to set
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @param numReparticao the numReparticao to set
     */
    public void setNumReparticao(int numReparticao) {
        this.numReparticao = numReparticao;
    }

    /**
     * @param codigoPostal the codigoPostal to set
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @Override
    public String toString() {
        return "Reparticao{" + "cidade=" + cidade + ", numReparticao=" + numReparticao + ", codigoPostal=" + codigoPostal + ", servicos=" + servicos + '}';
    }

}
