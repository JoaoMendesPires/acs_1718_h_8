/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atendimentoservicospublicos;

import auxiliares.DoublyLinkedList;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author João Pires <your.name at your.org>
 */
public class AtendimentoServicosPublicos {

    public DoublyLinkedList<Reparticao> listaReparticoes;
    public HashMap<Cidadao, Set<Senha>> listaSenhas;
    public ArrayList<Cidadao> listaCidadaos;

    public AtendimentoServicosPublicos() {
        this.listaReparticoes = new DoublyLinkedList<>();
        this.listaSenhas = new HashMap<>();
        this.listaCidadaos = new ArrayList();
    }

    public DoublyLinkedList<Reparticao> getListaReparticoes() {
        return listaReparticoes;
    }

    public void setListaReparticoes(DoublyLinkedList<Reparticao> listaReparticoes) {
        this.listaReparticoes = listaReparticoes;
    }

    public HashMap<Cidadao, Set<Senha>> getListaSenhas() {
        return listaSenhas;
    }

    public void setListaSenhas(HashMap<Cidadao, Set<Senha>> listaSenhas) {
        this.listaSenhas = listaSenhas;
    }

    public ArrayList<Cidadao> getListaCidadaos() {
        return listaCidadaos;
    }

    public void setListaCidadaos(ArrayList<Cidadao> listaCidadaos) {
        this.listaCidadaos = listaCidadaos;
    }

    //alínea a
    public void lerFicheiroReparticoes(String fileName) throws FileNotFoundException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String linha;
            while ((linha = reader.readLine()) != null) {
                ArrayList<Servico> listaServicos = new ArrayList<>();
                String[] tmp = linha.split(",");
                Reparticao r = new Reparticao(tmp[0], Integer.parseInt(tmp[1]), tmp[2]);
                for (int i = 3; i < tmp.length; i++) {
                    listaServicos.add(new Servico(tmp[i]));
                }
                r.setServicos(listaServicos);
                listaReparticoes.addLast(r);
            }
            reader.close();
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", fileName);
            e.printStackTrace();
        }
    }

    public void lerFicheiroSenhas(String fileName) throws FileNotFoundException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String linha;
            Set<Senha> senhas = null;
            while ((linha = reader.readLine()) != null) {
                String[] tmp = linha.split(",");
                Senha s = new Senha(Integer.parseInt(tmp[2]), new Servico(tmp[1]));
                for (Cidadao c : listaCidadaos) {
                    if (c.getContribuinte() == Integer.parseInt(tmp[0]) && !listaSenhas.containsKey(c)) {
                        senhas = new HashSet<Senha>();
                        senhas.add(s);
                        listaSenhas.put(c, senhas);
                    }
                    if (c.getContribuinte() == Integer.parseInt(tmp[0]) && listaSenhas.containsKey(c)) {
                        senhas = listaSenhas.get(c);
                        senhas.add(s);
                        listaSenhas.put(c, senhas);
                    }
                }
            }
            reader.close();
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", fileName);
            e.printStackTrace();
        }
    }

    public void lerFicheiroCidadaos(String fileName) throws FileNotFoundException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String linha;
            while ((linha = reader.readLine()) != null) {
                String[] tmp = linha.split(",");
                getListaCidadaos().add(new Cidadao(Integer.parseInt(tmp[1]), tmp[0], tmp[2], tmp[3], Integer.parseInt(tmp[4])));
            }
            reader.close();
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", fileName);
            e.printStackTrace();
        }
    }

    //alínea b
    public void addReparticao(Reparticao r) {
        boolean flagRepetido = true;
        for (Reparticao rep : listaReparticoes) {
            if (r.equals(rep)) {
                flagRepetido = false;
            }
        }
        if (flagRepetido = true) {
            listaReparticoes.addLast(r);

            for (Cidadao c : listaCidadaos) {
                String[] tmp = c.getCodigoPostal().split("-");
                if (r.getCodigoPostal().equals(tmp[0])) {
                    c.setNumReparticao(r.getNumReparticao());
                }
            }
        }

    }

    //Alínea c
    public void removerReparticao(Reparticao reparticao) {
        int dif = Integer.MAX_VALUE;
        Reparticao copia = new Reparticao();
        ListIterator<Reparticao> iterator = this.getListaReparticoes().listIterator();

        if (!listaReparticoes.isEmpty()) {
            for (Reparticao re : listaReparticoes) {
                if (Math.abs(Integer.parseInt(re.getCodigoPostal().substring(0, 3)) - Integer.parseInt(reparticao.getCodigoPostal().substring(0, 3))) < dif) {
                    dif = Integer.parseInt(re.getCodigoPostal().substring(0, 3));
                    copia = re;
                }

                if (re.equals(reparticao)) {
                    iterator.remove();
                }
                iterator.next();
            }

            for (Cidadao c : listaCidadaos) {
                if (c.getNumReparticao() == reparticao.getNumReparticao()) {
                    c.setNumReparticao(copia.getNumReparticao());
                }
            }

        }
    }

    //Alínea d
    public HashMap getCidadaosReparticao() {
        HashMap<String, Set<Integer>> cidadaosReparticao = new HashMap<>();
        Set<Integer> listaCids = null;
        for (Reparticao r : listaReparticoes) {
            for (Cidadao c : listaCidadaos) {
                String s = r.getNumReparticao() + " " + r.getCidade();
                if (r.getNumReparticao() == c.getNumReparticao() && !cidadaosReparticao.containsKey(s)) {
                    listaCids = new HashSet<Integer>();
                    listaCids.add(c.getContribuinte());
                    cidadaosReparticao.put(s, listaCids);
                }
                if (r.getNumReparticao() == c.getNumReparticao() && cidadaosReparticao.containsKey(s)) {
                    listaCids = cidadaosReparticao.get(s);
                    listaCids.add(c.getContribuinte());
                    cidadaosReparticao.put(s, listaCids);
                }
            }
        }
        return cidadaosReparticao;
    }

    //Alinea e
    public void inserirCidadao(Cidadao cidadao) {
        boolean flag = false;
        for (Cidadao c : listaCidadaos) {
            if (cidadao.getContribuinte() == c.getContribuinte()) {
                flag = true;
            }
        }

        if (flag == false) {
            listaCidadaos.add(cidadao);
        }
    }

    //alínea f
    public int getCidadaoFila(Reparticao r, int horas, int minutos) {
        int tempoAbertura = horasToMinutos(9, 0);
        int tempoFecho = horasToMinutos(15, 30);
        int tempoAtual = horasToMinutos(horas, minutos);

        ArrayList<Integer> numOrdemSenhas;

        if (tempoAtual >= tempoAbertura && tempoAtual <= tempoFecho) {
            for (Map.Entry<Cidadao, Set<Senha>> senhasCid : listaSenhas.entrySet()) {
                numOrdemSenhas = new ArrayList();
                for (Senha s : senhasCid.getValue()) {
                    numOrdemSenhas.add(s.getNumOrdem());
                }
                int max = Collections.max(numOrdemSenhas);
                int tempoEspera = max * 10 + ((Collections.frequency(numOrdemSenhas, max) - 1) * 10);
                if (r.getNumReparticao() == senhasCid.getKey().getNumReparticao()) {
                    System.out.println(senhasCid.getKey().getContribuinte() + " esperou: " + tempoEspera + " minutos");
                    return tempoEspera;
                }
            }
        } else {
            throw new IllegalArgumentException("Repartição está fechada!");
        }
        return 0;
    }

    public int horasToMinutos(int horas, int minutos) {
        if (horas < 24 && minutos < 60) {
            return horas * 60 + minutos;
        }
        throw new IllegalArgumentException("Horas inválidas");
    }

    //alínea g
    public HashMap servicosMaiorProcura() {

        HashMap<String, Integer> m = new HashMap<>();
        ArrayList<String> listaServicos = new ArrayList();

        for (Map.Entry<Cidadao, Set<Senha>> senhasCid : listaSenhas.entrySet()) {
            for (Senha s : senhasCid.getValue()) {
                listaServicos.add(s.getCodigoAssunto().getIdServico());
            }
        }

        Collections.sort(listaServicos);

        String aux = "";
        for (String servico : listaServicos) {
            if (!aux.equals(servico)) {
                m.put(servico, Collections.frequency(listaServicos, servico));
                aux = servico;
            }
        }
        return m;
    }

    //Alinea h
    public void abandonarFila(Reparticao r, int horaAtual, int minutoAtual, Cidadao cidadao) {
        int horaFecho = 15;
        int minutoFecho = 30;

        for (Map.Entry<Cidadao, Set<Senha>> senhasCid : listaSenhas.entrySet()) {
            if (cidadao == senhasCid.getKey() && getCidadaoFila(r, horaFecho, minutoFecho) == getCidadaoFila(r, horaAtual, minutoAtual)) {
                listaSenhas.remove(cidadao);
            }
        }
    }
}
