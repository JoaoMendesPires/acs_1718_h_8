/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atendimentoservicospublicos;

/**
 *
 * @author João Pires <your.name at your.org>
 */
public class Senha {

    private int numOrdem;
    private Servico codigoAssunto;

    private static final int NUM_ORDEM_POR_OMISSAO = 0;
    private static final Servico CODIGO_ASSUNTO_POR_OMISSAO = new Servico("");

    public Senha(int numOrdem, Servico codigoAssunto) {
        this.numOrdem = numOrdem;
        this.codigoAssunto = codigoAssunto;
    }

    public Senha() {
        numOrdem = NUM_ORDEM_POR_OMISSAO;
        codigoAssunto = CODIGO_ASSUNTO_POR_OMISSAO;
    }

    /**
     * @return the numOrdem
     */
    public int getNumOrdem() {
        return numOrdem;
    }

    /**
     * @return the codigoAssunto
     */
    public Servico getCodigoAssunto() {
        return codigoAssunto;
    }

    /**
     * @param numOrdem the numOrdem to set
     */
    public void setNumOrdem(int numOrdem) {
        this.numOrdem = numOrdem;
    }

    /**
     * @param codigoAssunto the codigoAssunto to set
     */
    public void setCodigoAssunto(Servico codigoAssunto) {
        this.codigoAssunto = codigoAssunto;
    }

    @Override
    public String toString() {
        return "Senha{" + "numOrdem=" + numOrdem + ", codigoAssunto=" + codigoAssunto + '}';
    }

}
