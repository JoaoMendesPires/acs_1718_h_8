/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atendimentoservicospublicos;

/**
 *
 * @author Henrique Maciel <1150863@isep.ipp.pt>
 */
public class Servico {

    private String idServico;

    public Servico(String idServico) {
        this.idServico = idServico;
    }

    public String getIdServico() {
        return idServico;
    }

    public void setIdServico(String idServico) {
        this.idServico = idServico;
    }

    @Override
    public String toString() {
        return "Servico{" + "idServico=" + idServico + '}';
    }

}
