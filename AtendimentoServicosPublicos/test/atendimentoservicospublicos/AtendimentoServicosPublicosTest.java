/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atendimentoservicospublicos;

import auxiliares.DoublyLinkedList;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Henrique Maciel <1150863@isep.ipp.pt>
 */
public class AtendimentoServicosPublicosTest {

    AtendimentoServicosPublicos instance = new AtendimentoServicosPublicos();

    public AtendimentoServicosPublicosTest() {
    }

    /**
     * Test of getListaReparticoes method, of class AtendimentoServicosPublicos.
     */
    @Test
    public void testGetListaReparticoes() {
        System.out.println("getListaReparticoes");
        DoublyLinkedList<Reparticao> expResult = new DoublyLinkedList<>();
        DoublyLinkedList<Reparticao> result = instance.getListaReparticoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaReparticoes method, of class AtendimentoServicosPublicos.
     */
    @Test
    public void testSetListaReparticoes() {
        System.out.println("setListaReparticoes");
        DoublyLinkedList<Reparticao> listaReparticoes = null;
        instance.setListaReparticoes(listaReparticoes);
    }

    /**
     * Test of getListaSenhas method, of class AtendimentoServicosPublicos.
     */
    @Test
    public void testGetListaSenhas() {
        System.out.println("getListaSenhas");
        HashMap<Cidadao, Set<Senha>> expResult = new HashMap<>();
        HashMap<Cidadao, Set<Senha>> result = instance.getListaSenhas();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaSenhas method, of class AtendimentoServicosPublicos.
     */
    @Test
    public void testSetListaSenhas() {
        System.out.println("setListaSenhas");
        HashMap<Cidadao, Set<Senha>> listaSenhas = null;
        instance.setListaSenhas(listaSenhas);
    }

    /**
     * Test of getListaCidadaos method, of class AtendimentoServicosPublicos.
     */
    @Test
    public void testGetListaCidadaos() {
        System.out.println("getListaCidadaos");
        ArrayList<Cidadao> expResult = new ArrayList();
        ArrayList<Cidadao> result = instance.getListaCidadaos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaCidadaos method, of class AtendimentoServicosPublicos.
     */
    @Test
    public void testSetListaCidadaos() {
        System.out.println("setListaCidadaos");
        ArrayList<Cidadao> listaCidadaos = null;
        instance.setListaCidadaos(listaCidadaos);
    }

    /**
     * Test of lerFicheiroReparticoes method, of class
     * AtendimentoServicosPublicos.
     *
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testLerFicheiroReparticoes() throws FileNotFoundException {
        System.out.println("lerFicheiroReparticoes");
        instance.lerFicheiroReparticoes("fx_reparticoes.txt");
        Reparticao expected = new Reparticao("Porto", 1234, "4200");
        expected.addServico("A");
        expected.addServico("C");
        expected.addServico("D");
        ListIterator<Reparticao> iterator = instance.getListaReparticoes().listIterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next().toString());
        }

        assertEquals(instance.getListaReparticoes().first().toString(), expected.toString());
    }

    /**
     * Test of lerFicheiroCidadaos method, of class AtendimentoServicosPublicos.
     *
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testLerFicheiroCidadaos() throws FileNotFoundException {
        System.out.println("lerFicheiroCidadaos");
        instance.lerFicheiroCidadaos("fx_cidadaos.txt");
        Cidadao expected = new Cidadao(111222333, "Ana", "ana@gmail.com", "4200-072", 1234);

        int i = 0;
        while (instance.getListaCidadaos().size() > i) {
            System.out.println(instance.getListaCidadaos().get(i).toString());
            i++;
        }
        assertEquals(instance.getListaCidadaos().get(0).toString(), expected.toString());
    }

    /**
     * Test of lerFicheiroSenhas method, of class AtendimentoServicosPublicos.
     *
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testLerFicheiroSenhas() throws FileNotFoundException {
        System.out.println("lerFicheiroSenhas");

        instance.lerFicheiroCidadaos("fx_cidadaos.txt");
        instance.lerFicheiroSenhas("fx_senhas.txt");

        Cidadao expected = new Cidadao(111222333, "Ana", "ana@gmail.com", "4200-072", 1234);

        for (Map.Entry<Cidadao, Set<Senha>> senhasCid : instance.listaSenhas.entrySet()) {
            for (Senha s : senhasCid.getValue()) {
                System.out.println(senhasCid.getKey().getContribuinte());
                System.out.println(s.toString());

            }
        }
        //  assertEquals(instance.getListaSenhas().get().toString(), expected.toString());
    }

    @Test
    public void testAddReparticao() throws FileNotFoundException {
        System.out.println("addReparticao");
        instance.lerFicheiroReparticoes("fx_reparticoes.txt");
        instance.lerFicheiroCidadaos("fx_cidadaos.txt");
        Reparticao expected = new Reparticao("V.N. de Gaia", 1357, "4430");
        expected.addServico("A");
        expected.addServico("D");
        instance.addReparticao(expected);
        System.out.println(instance.getListaReparticoes().last().toString());
        System.out.println("Novo num repatição do Enrique: " + instance.getListaCidadaos().get(3).getNumReparticao());

        assertEquals(instance.getListaReparticoes().last().toString(), expected.toString());
    }

    @Test
    public void testInserirCidadao() throws FileNotFoundException {
        System.out.println("InserirCidadao");
        instance.lerFicheiroCidadaos("fx_cidadaos.txt");
        Cidadao expected = new Cidadao(141252363, "José Socrates", "js@gmail.com", "3456-234", 2);
        instance.inserirCidadao(expected);
        assertEquals(instance.getListaCidadaos().get(instance.getListaCidadaos().size() - 1).toString(), expected.toString());
    }

//    @Test
//    public void testRemoverReparticao() throws FileNotFoundException {
//        System.out.println("RemoverRepartição");
//        AtendimentoServicosPublicos instance = new AtendimentoServicosPublicos();
//        instance.lerFicheiroReparticoes("fx_reparticoes.txt");
//        instance.lerFicheiroCidadaos("fx_cidadaos.txt");
//        DoublyLinkedList<Reparticao> reparticoes = instance.getListaReparticoes();
//        ArrayList<Cidadao> cidadaos = instance.getListaCidadaos();
//
//    }
    @Test
    public void testGetCidadaosReparticao() throws FileNotFoundException {

        System.out.println("getCidadaosReparticao");
        instance.lerFicheiroReparticoes("fx_reparticoes.txt");
        instance.lerFicheiroCidadaos("fx_cidadaos.txt");
        HashMap<String, Set<Integer>> m = instance.getCidadaosReparticao();

        Integer expected = 111222333;

        for (Map.Entry<String, Set<Integer>> map : m.entrySet()) {
            for (Integer i : map.getValue()) {
                System.out.println(map.getKey());
                System.out.println(i);
            }
        }
        //assertTrue(m.containsValue(expected));
    }

    @Test
    public void testGetCidadaoFila() throws FileNotFoundException {

        System.out.println("getCidadaoFila");
        AtendimentoServicosPublicos instance = new AtendimentoServicosPublicos();
        instance.lerFicheiroReparticoes("fx_reparticoes.txt");
        instance.lerFicheiroCidadaos("fx_cidadaos.txt");
        instance.lerFicheiroSenhas("fx_senhas.txt");
        for (Reparticao r : instance.listaReparticoes) {
            // System.out.println(r.getNumReparticao());
            instance.getCidadaoFila(r, 9, 40);
        }
        int expected = 20;
        Reparticao rep = instance.getListaReparticoes().first();
        System.out.println("AssertEquals getCidadaoFila");
        assertEquals(instance.getCidadaoFila(rep, 9, 40), expected);
    }

    @Test
    public void testServicosMaiorProcura() throws FileNotFoundException {

        System.out.println("ServicosMaiorProcura");
        instance.lerFicheiroCidadaos("fx_cidadaos.txt");
        instance.lerFicheiroSenhas("fx_senhas.txt");
        HashMap<String, Integer> map = instance.servicosMaiorProcura();
        System.out.println("Serviços com maior procura: ");
        for (Map.Entry m : map.entrySet()) {
            System.out.println(m.getKey() + ", " + m.getValue());
        }

        // assertEquals(instance.getListaReparticoes().first().toString(), expected.toString());
    }

    @Test
    public void testAbandonarFila() throws FileNotFoundException {
        System.out.println("abandonarFila");

        instance.lerFicheiroCidadaos("fx_cidadaos.txt");
        instance.lerFicheiroSenhas("fx_senhas.txt");
        instance.lerFicheiroReparticoes("fx_reparticoes.txt");

        instance.abandonarFila(instance.listaReparticoes.first(), 10, 0, instance.listaCidadaos.get(0));

        for (Map.Entry<Cidadao, Set<Senha>> senhasCid : instance.listaSenhas.entrySet()) {
            for (Senha s : senhasCid.getValue()) {
                System.out.println(senhasCid.getKey().getContribuinte());
                System.out.println(s.toString());

            }
        }
        //  assertEquals(instance.getListaSenhas().get().toString(), expected.toString());
    }
}
